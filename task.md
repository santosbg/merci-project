# PhotoCarrot final tuches

## The short story

A small company has a website where users can share images and posts. The website is almost ready when the developer who's working on it gets sick. Your job is to finish the website. You can see how should it look like from the example-view.png in the assets folder.

## Here is what you will train
1. pure functions
2. HTML 
3. CSS

## What you won't train 
1. DOM manipulation

## Tasks
1. Add all the necessary HTML elements.  
1.1 Find an icon with a carrot at fontawesome.com.  
1.2 Add a search bar.  
1.3 Add a profile picture from the assets folder.  
1.4 Add 3 text fields containing info about the user activity.  
1.5 One of the images is not showing. Find out why and fix it.  

    **Notes**  
    - Font Awesome is a library which gives you free nice looking icons
    - When creating an element please check in style.css if there is an existing id or class for the element (there is, for all of them :D).

2. Add all CSS styles
    **Notes**  
    - use google fonts if you need a really fancy font

3. There are 2 sorting functions in the utils.js which should be implemented
    **Notes**  
    - They must be pure functions
    - Check the 'Date' object in javascript

4. Replace with semantic HTML (additional task)
- check on the web about the semantic HTML elements and see where can you use them in the project.

**Everything else needed for the website to work, is implemented. If you manage to finish the full task correctly, you are more then welcome to add your functionality, elements and styles.**

