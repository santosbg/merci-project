export const images = [
    {
        source: './assets/1.png',
        likes: 10,
        uploadDate: '2015-03-25',
    },
    {
        source: './assets/2.png',
        likes: 9,
        uploadDate: '2015-10-30',
    },
    {
        source: './assets/3.png',
        likes: 6,
        uploadDate: '2015-02-20',
    },
    {
        source: './assets/4.png',
        likes: 3,
        uploadDate: '2015-01-10',
    },
    {
        source: './assets/5.png',
        likes: 2,
        uploadDate: '2015-03-02',
    },
    {
        source: './assets/6.png',
        likes: 1,
        uploadDate: '2015-02-28',
    },
];