import { images } from './data/images.js';
import * as utils from './utils.js';
import * as events from './events.js';
import * as views from './view.js';

(() => {
    // All the work here is done
    events.sortByDate(() => {
        const $div = $('#images-container');
        $div.empty();
        utils.sortImgsByDate(images).forEach(curData => {
            $div.append(`<img class="images" src="${curData.source}">`);
        });
    });
    events.sortByLikes(() => {
        const $div = $('#images-container');
        $div.empty();
        utils.sortImgsByLikes(images).forEach(curData => {
            $div.append(`<img class="images" src="${curData.source}">`);
        });
    });

})();