// All the work here is done

const sortByDate = (callback) => {
    $('#sortByDateBtn').on('click', callback);
};

const sortByLikes = (callback) => {
    $('#sortByLikesBtn').on('click', callback);
};

export {
    sortByDate,
    sortByLikes
}
